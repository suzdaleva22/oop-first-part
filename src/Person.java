    class Person {
    private String name;
    private int age;
    static int numberOfPersons = 0;

    // Конструктор класса Person
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
        numberOfPersons++;
    }

    // Метод для получения имени
    public String getName() {
        return name;
    }
    public int getAge() {
        return age;
    }
    public static int getNumberOfUsers() {
        return numberOfPersons;
    }

    // Метод для установки возраста
    public void setAge(int age) {
        this.age = age;
    }

    // Абстрактный метод
   // abstract void login();

    // Вложенный класс
    class Nested{
        void print() {

            System.out.println("Nested class Person: " + name);
        }
    }
    //Вложенный статический класс
    static class NestedStatic{
        void print() {
            System.out.println("Nested static class Person");
        }
    }
    public void localClass() {
        class Local{
            void printMessage() {
                System.out.println("Local class inside Person");
            }
        }
        Local local = new Local();
        local.printMessage();
    }
//Анонимный класс
    interface Access {
        void action();
    }
    void performAction() {
        Access access = new Access() {
            public void action() {
                System.out.println("Anonim class: " + name);
            }
        };
        access.action();
    }




}