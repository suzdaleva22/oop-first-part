
public class Main {
    public static void main(String[] args) {

       Person admin = new Person("Alina", 22);
        admin.setAge(30);
        System.out.println("PublicClass: Name = " + admin.getName() + ", Age = " + admin.getAge());

        Person superadmin = new Person("Alex", 38);
        System.out.println("PublicClass: Name = " + superadmin.getName() + ", Age = " + superadmin.getAge());
        System.out.println("Number of Users: " + Person.getNumberOfUsers());

        Person.Nested nested = admin.new Nested();
        nested.print();

        Person.NestedStatic nestedStatic = new Person.NestedStatic();
        nestedStatic.print();

        admin.localClass();
        superadmin.performAction();
    }


}